import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  data: {
    playerName: undefined
  },
  mounted: function() {
    const vueapp = this;
    window.addEventListener('keyup', (event) => {
      if (event.keyCode == 40 || event.keyCode == 98) { 
        vueapp.$emit('down-arrow-pressed');
      } else if (event.keyCode == 39 || event.keyCode == 102) {
        vueapp.$emit('right-arrow-pressed');
      } else if (event.keyCode == 38 || event.keyCode == 104) {
        vueapp.$emit('up-arrow-pressed');
      } else if (event.keyCode == 37 || event.keyCode == 100) {
        vueapp.$emit('left-arrow-pressed');
      } else if (event.keyCode == 103 || event.keyCode == 36) {
        vueapp.$emit('upleft-arrow-pressed');
      } else if (event.keyCode == 105 || event.keyCode == 33) {
        vueapp.$emit('upright-arrow-pressed');
      } else if (event.keyCode == 97 || event.keyCode == 35) {
        vueapp.$emit('downleft-arrow-pressed');
      } else if (event.keyCode == 99 || event.keyCode == 34) {
        vueapp.$emit('downright-arrow-pressed');
      } else if (event.keyCode == 101 || event.keyCode == 12) {
        vueapp.$emit('self-pressed');
      }
    });
  }
}).$mount('#app')
